﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace May_18th_project
{
    public class Count
    {
        //加法
        public int Add(int num1, int num2)
        {
            return num1 + num2;
        }
        //减法
        public int Minus(int num1, int num2)
        {
            return num1 - num2;
        }
        //乘法
        public int Multiply(int num1, int num2)
        {
            return num1 * num2;
        }
        //除法
        public int Divide(int num1, int num2)
        {
            return num1 / num2;
        }
    }
}
