﻿
using System;

namespace App
{
    class Program
    {
        public void Eat()
        {
            Console.WriteLine("他们在一起吃饭");
        }
        public void Play()
        {
            Console.WriteLine("他们在一起玩耍");
        }

        /// <summary>
        /// 加法
        /// </summary>
        /// <param name="number1"></param>
        /// <param name="number2"></param>
        /// <returns></returns>
        public double Add(double number1,double number2)
        {
            return number1 + number2;
        }
        /// <summary>
        /// 减法
        /// </summary>
        /// <returns></returns>
        public double Sub(double number1,double number2)
        {
            return number2 - number1;
        }
        /// <summary>
        /// 乘法
        /// </summary>
        /// <returns></returns>
        public double Mul(double number1,double number2)
        {
            return number1 * number2;
        }
        /// <summary>
        /// 除法
        /// </summary>
        /// <returns></returns>
        public double Div(double number1,double number2)
        {
            return number2 / number1;
        }
    }
}
