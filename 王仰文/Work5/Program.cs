﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work5
{
    class Program
    {
        static void Main(string[] args)
        {
            Compute compute = new Compute();

            var jia = compute.Add(3 ,2.2);

            var jian = compute.Minus(999, 0.0005);

            var cheng = compute.Multiply(24, 5.0);

            var chu = compute.Divide(1998, 3);


            Console.WriteLine("两数之和为："+jia.ToString("0.00"));
            Console.WriteLine("两数之差为：" + jian.ToString());
            Console.WriteLine("两数之积为：" + cheng.ToString("0.0"));
            Console.WriteLine("两数之商为：" + chu.ToString());
            
        }
    }
}
