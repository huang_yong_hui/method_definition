﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Work5
{
    class Compute
    {
        //加法
        public double Add(double num1,double num2)
        {
            return num1 + num2;
        }
        //减法
        public double Minus(double num3, double num4)
        {
            return num3 - num4;
        }
        //乘法
        public double Multiply(double num5, double num6)
        {
            return num5 * num6;
        }
        //除法
        public double Divide(double num7, double num8)
        {
            return num7 / num8;
        }

    }
}
