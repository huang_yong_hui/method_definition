﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Definition
{
   public class Compute
    {
        public double Add(double num1, double num2)
        {
            return num1 + num2;
        }
        internal int Minus(int num1,int num2)
        {
            return num1 - num2;
        }
        public long Multiply(long num1,long num2)
        {
            return num1 * num2;
        }
        internal decimal Divide(decimal num1,decimal num2)
        {
            return num1 / num2;
        }
    }
}
