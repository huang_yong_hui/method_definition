﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Refclass refclass = new Refclass();
            int a = 20;
            bool rs = refclass.Judge(ref a);
            Console.WriteLine(rs);
            OutClass outClass = new OutClass();
            bool rs1;
            outClass.Judge1(30, out rs1);
            Console.WriteLine(rs1);
        }
    }
}
