﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class OutClass
    {
        public void Judge1(int num1 , out bool result)
        {
            if (num1 % 5 == 0)
            {
                result = true;
            }
            else
            {
                result = false;
            }
        }
    }
}
