﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Compute compute = new Compute();

            var add=compute.Add(2, 3);
            Console.WriteLine("加完等于{0}", add);


            var minus = compute.Minus(3,2);
            Console.WriteLine("减完等于{0}", minus);


            var multiply = compute.Multiply(2, 5);
            Console.WriteLine("乘完等于{0}", multiply);


            var divide = compute.Divide(4, 2);
            Console.WriteLine("除完等于{0}", divide);

        }
    }
}
