﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ConsoleApp1
{
    class Compute
    {
        //加法
        public double Add(double num1, double num2)
        {
            return num1 + num2;
        }
        //减法
        public double Minus(double num1, double num2)
        {
            return num1 - num2;
        }
        //乘法
        public double Multiply(double num1, double num2)
        {
            return num1 * num2;
        }
        //除法
        public double Divide(double num1, double num2)
        {
            return num1 / num2;
        }
    }
}
