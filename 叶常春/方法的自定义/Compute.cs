﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 方法的自定义
{
    class Compute
    {
        
        public void Addition(int num1,int num2)
        {
            Console.WriteLine("{0}+{1}的和为{2}",num1,num2,num1+num2);
        }

          public void Subtraction(int num1, int num2)
        {
            Console.WriteLine("{0}-{1}的差为{2}", num1, num2, num1-num2);
        }

        public void Multiplication(int num1, int num2)
        {
            Console.WriteLine("{0}*{1}的积为{2}", num1, num2, num1*num2);
        }

        public void DivisionMethod(int num1, int num2)
        {
            Console.WriteLine("{0}/{1}的商为{2}", num1, num2, num1/num2);
        }


    }
}
