﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 方法的自定义
{
    class Input
    {
        public static void Begin()
        {
            int num1;
            int num2;
            Compute compute = new Compute();
            A:
            try {
                Console.WriteLine("请输入第一个数");
                 num1 = Convert.ToInt32(Console.ReadLine());
            }//try
            catch 
            {
               
                Console.WriteLine("你的输入有误");
                goto A;
            }//catch
             




            Console.WriteLine("请输入运算符号");
            string symbol = Console.ReadLine();

            while (true)
            {
                if (symbol == "+" || symbol == "-" || symbol == "*" || symbol == "/")
                {
                    break;
                }
                else
                { 
                        Console.WriteLine("你的输入有误，请重新输入");
                     symbol = Console.ReadLine();

                }
            }
        B:
            try
            {
                Console.WriteLine("请输入第二个数");
                 num2 = Convert.ToInt32(Console.ReadLine());
            }
            catch 
            {
                Console.WriteLine("你的输入有误");
                 goto B;
            }

            switch (symbol)
            {
                case "+":compute.Addition(num1,num2);break;
                case "-":compute.Subtraction(num1, num2);break;
                case "*": compute.Multiplication(num1, num2); break;
                case "/": compute.DivisionMethod(num1, num2); break;
            }
               


        }
    }
}
