﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    class T5
    {

        public double Add(double x, double y)
        {
            return x + y;
        }
 
        public int Sub(int x, int y)
        {
            return x - y;
        }
       
        public int Mul(int x, int y)
        {
            return x * y;
        }
      
        public double Div(double x, double y)
        {
            return x / y;
        }

        public int Rem(int x, int y)
        {
            return x % y;
        }

    }
}