﻿using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            T5 textOne = new T5();

            var add = textOne.Add(8.6, 5.2);

            var div = textOne.Div(4.5, 1.8);

            var mul = textOne.Mul(9, 2);

            var rem = textOne.Rem(3, 7);

            var sub = textOne.Sub(8, 7);

            Console.WriteLine("加法为：{0}减法为：{1}乘法为：{2}除法为：{3}取余为：{4}", add, sub, mul, div, rem);
        }
    }
}