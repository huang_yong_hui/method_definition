﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Method
{
    class Compute01
    {
        //加法
        public double Add(double num1, double num2)
        {
            return num1 + num2;
        }

        //减法
        public double Minus(double num1, double num2)
        {
            return num1 - num2;
        }

        //乘法
        public double Multiply(double num1, double num2)
        {
            return num1 * num2;
        }

        //除法
        public double Divide(double num1, double num2)
        {
            return num1 / num2;
        }

        //求余
        public int Remainder(int num1, int num2)
        {
            return num1 % num2;
        }

        //阶乘
        public int Factorial(int num)

        {
            int result;
            if (num == 1)
            {
                return 1;
            }
            else
            {
                result = Factorial(num - 1) * num;
            }
            return result;
        }
    }
}
