﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Method
{
    class Compute02
    {
        static void Main(String[] args)
        {
            Compute01 compute = new Compute01();

            var add = compute.Add(1.47, 4.36);


            var minus = compute.Minus(6.14, 2.75);


            var multiply = compute.Multiply(3.82, 5);


            var divide = compute.Divide(9, 6);


            var remainder = compute.Remainder(7, 5);


            var factorial = compute.Factorial(3);


            Console.WriteLine("加法计算结果：{0}，减法计算结果：{1}，乘法计算结果：{2}，除法计算结果：{3}，余数计算结果：{4}，3的阶乘：{5}", add, minus, multiply, divide, remainder, factorial);

            Console.ReadKey();

        }
    }
}
