﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Algorithm algorithm = new Algorithm();
            var add = algorithm.Add(10.1, 6);
            Console.WriteLine("加法结果为：{0}", add);
            var minus = algorithm.Minus (6.6, 0.6);
            Console.WriteLine("减法结果为：{0}", minus );
            var multiply = algorithm.Multiply(3, 9);
            Console.WriteLine("乘法结果为：{0}",multiply );
            var divide = algorithm.Divide (81, 3);
            Console.WriteLine("除法结果为：{0}", divide );
        }
    }
}
