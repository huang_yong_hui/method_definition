﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Compute compute = new Compute();
            var test1 = compute.Add(2.0, 8.0);
            var test2 = compute.Minus(10.0, 6.0);
            var test3 = compute.Multiply(20.0, 10.0);
            var test4 = compute.Divide(80.0, 20.0);
            Console.WriteLine(test1);
            Console.WriteLine(test2);
            Console.WriteLine(test3);
            Console.WriteLine(test4);
        }
    }
}
