﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 方法定义
{
    class Computer
    {
        //+
        public double Up(double the1,double the2)
        {
            return the1 + the2;
        }
        //减
        public double Down(double num1, double num2)
        {
            return num1 - num2;
        }
        //乘
        public double Multiply(double num1, double num2)
        {
            return num1 * num2;
        }
        //除
        public double Divide(double num1, double num2)
        {
            return num1 / num2;
        }
    }
}
