﻿using System;

namespace 方法定义
{
    class Program
    {
        static void Main(string[] args)
        {
            Computer computer = new Computer();
            var one = computer.Up(2, 3);
            Console.WriteLine(one);
            var two = computer.Down(3, 4);
            Console.WriteLine(two);
            var three = computer.Multiply(5, 5);
            Console.WriteLine(three);
            var four = computer.Divide(6, 3);
            Console.WriteLine(four);
        }
    }

    
}
