﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Compute compute = new Compute();
            //加法
            var add = compute.Add(1.5, 2);
            Console.WriteLine(add);
            //减法
            var min = compute.Min(7.5, 12);
            Console.WriteLine(min);
            //乘法
            var mul = compute.Mul(1.3, 2.5);
            Console.WriteLine(mul);
            //除法
            var div = compute.Div(35.4, 22);
            Console.WriteLine(div);
        }
    }
}
