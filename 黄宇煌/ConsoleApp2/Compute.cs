﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp2
{
    class Compute
    {
        //加法
        public double Add(double num1, double num2)
        {
            return num1 + num2;
        }
        //减法
        public double Min(double num1, double num2)
        {
            return num1 - num2;
        }
        //惩罚
        public double Mul(double num1, double num2)
        {
            return num1 * num2;
        }
        //除法
        public double Div(double num1, double num2)
        {
            return num1 / num2;
        }
    }
}
