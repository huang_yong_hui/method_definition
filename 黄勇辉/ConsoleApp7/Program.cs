﻿using System;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            Compute compute = new Compute();
            var res = compute.Plus(9, 3);
            Console.WriteLine(res);
            var res2 = compute.Reduce(8, 11);
            Console.WriteLine(res2);
            var res3 = compute.Multiply(18, 5);
            Console.WriteLine(res3);
            var res4 = compute.Divide(32, 4);
            Console.WriteLine(res4);
        }
    }
}