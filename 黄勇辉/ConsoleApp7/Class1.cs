﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp7
{
    public class Compute
    {
        public int Plus(int num1, int num2)
        {
            return num1 + num2;
        }
        public int Reduce(int num1, int num2)
        {
            return num1 - num2;
        }
        public double Multiply(double num1, double num2)
        {
            return num1 * num2;
        }
        public double Divide(double num1, double num2)
        {
            return num1 / num2;
        }
    }
}