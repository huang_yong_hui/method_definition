﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   class Compute
    {
        public int Add(int num1,int num2)
        {
            return num1 + num2;
        }
        static void Main(string[] args)
        {
            Compute compute = new Compute();
            var sum = compute.Add(4,5);
            Console.WriteLine(sum);
        }
    }
    
}
