﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Computer_方法
{
    class Program
    {
        static void Main(string[] args)
        {
            //实例化
            computer compute = new computer();
            double num1;
            double num2;

            num1 = 97.23;
            num2 = 36.37;

            //加法
            var add = compute.Add(num1, num2);
            Console.WriteLine("{0} + {1} = {2}", num1, num2, add);
            //减法
            var min = compute.Min(num1, num2);
            Console.WriteLine("{0} * {1} = {2}", num1, num2, min);
            //乘法
            var mul = compute.Mul(num1, num2);
            Console.WriteLine("{0} * {1} = {2}", num1, num2, mul);
            //除法
            var div = compute.Div(num1, num2);
            Console.WriteLine("{0} ÷ {1} = {2}", num1, num2, mul);


        }
    }
}
