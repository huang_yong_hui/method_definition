﻿using System;

namespace Csharp第五次作业
{
    class Program
    {
        static void Main(string[] args)
        {
            Computer computer = new Computer();
            Console.WriteLine("加法:" + computer.Addition(10, 20));
            Console.WriteLine("减法:" + computer.Subtraction(20, 10));
            Console.WriteLine("乘法:" + computer.Multiplication(3, 5));
            Console.WriteLine("除法:" + computer.Division(15, 5));
            Console.ReadKey();
        }
    }
}
