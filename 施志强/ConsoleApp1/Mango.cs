﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Schema;

namespace ConsoleApp1
{
     public class Mango
    {
        public double Add(double n1,double n2)
        {
            return n1 + n2;
        }
        public double Min(double n1,double n2)
        {
            return n1 - n2;
        }
        public double Mul(double n1,double n2)
        {
            return n1 * n2;
        }
        public double Div(double n1,double n2)
        {
            return n1 / n2;
        }
    }
}
