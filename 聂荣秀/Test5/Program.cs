﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test5
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Calculator calculator = new Calculator();
            var res = calculator.Add(3.14,3.0);
            Console.WriteLine("3.14+3.0=" + res);

            var mus = calculator.Minus(58.64, 27.77);
            Console.WriteLine("58.64-27.77=" + mus);

            var mby = calculator.Minus(11, 11.0);
            Console.WriteLine("11*11.0=" + mby);

            var div = calculator.Minus(81, 9);
            Console.WriteLine("81/9=" + div);



        }
    }
}
