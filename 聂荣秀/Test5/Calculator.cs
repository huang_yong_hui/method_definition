﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test5
{
    class Calculator
    {
        public double Add (double numA,double numB)
        {
            return numA + numB;
        }

        public double Minus (double numA, double numB)
        {
            return numA - numB;
        }

        public double Multiplied (double numA, double numB)
        {
            return numA * numB;
        }

        public double Divide (double numA, double numB)
        {
            return numA / numB;
        }

    }
}
