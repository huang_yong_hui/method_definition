﻿using System;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            Compute compute = new Compute();
            var res = compute.Add(22, 33);
            Console.WriteLine(res);
            var res2 = compute.Minus(22, 33);
            Console.WriteLine(res2);
            var res3 = compute.Multiply(22, 33);
            Console.WriteLine(res3);
            var res4 = compute.Divide(22, 33);
            Console.WriteLine(res4);
        }
    }
}
