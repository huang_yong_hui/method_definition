﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DefineMethods
{
    class OperatorOverLoading
    {
        public double x, y, z;
        public OperatorOverLoading(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public OperatorOverLoading(OperatorOverLoading rhs)
        {
            x = rhs.x;
            y = rhs.y;
            z = rhs.z;
        }
        public override string ToString()
        {
            return "{" + x + " " + y + " " + z + "}";
        }
        public static OperatorOverLoading operator +(OperatorOverLoading lhs, OperatorOverLoading rhs)
        {
            OperatorOverLoading result = new OperatorOverLoading(lhs);
            result.x += rhs.x;
            result.y += rhs.y;
            result.z += rhs.z;
            return result;
        }

        internal bool toString()
        {
            throw new NotImplementedException();
        }
    }
}
