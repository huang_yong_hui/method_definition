﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DefineMethods
{
    class NumberInput
    {
        public void getValues(out int x, out int y)
        {
            Console.WriteLine("请输入第一个值： ");
            x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("请输入第二个值： ");
            y = Convert.ToInt32(Console.ReadLine());
        }
    }
}
