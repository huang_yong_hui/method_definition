﻿using System;

namespace DefineMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberInput n = new NumberInput();
            Recursion recursion = new Recursion();
             
            int a, b;

            /* 调用函数来获取值 */
            n.getValues(out a, out b);

            Console.WriteLine("在方法调用之后，a 的值： {0}", a);
            Console.WriteLine("在方法调用之后，b 的值： {0}", b);


            Console.WriteLine("6 的阶乘是： {0}", recursion.factorial(6));
            Console.WriteLine("7 的阶乘是： {0}", recursion.factorial(7));
            Console.WriteLine("8 的阶乘是： {0}", recursion.factorial(8));
            Console.ReadLine();


            OperatorOverLoading vect1, vect2, vect3;
            vect1 = new OperatorOverLoading(3.0, 3.0, 1.0);
            vect2 = new OperatorOverLoading(2.0, -4.0, -4.0);
            vect3 = vect1 + vect2;
            Console.WriteLine(vect1.toString());
            Console.WriteLine(vect2.toString());
            Console.WriteLine(vect3.toString());
 
            Console.ReadLine();
        }
    }
}
