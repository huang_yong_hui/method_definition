﻿using System;

namespace ConsoleApp001
{
    class Program
    {
        public static void Main(string[] args)
        {
            Dog dog = new Dog();

            dog.Age();
            dog.Dance();
            dog.Dance();

            Compute compute = new Compute();

            compute.Text("章超群", 20);
            var text= compute.Text("章超群", 20);
            Console.WriteLine(text);

            compute.TextAdd("章超群", "130kg");
            var textAdd = compute.TextAdd("章超群", "130kg");
            Console.WriteLine(textAdd);

            compute.Add(12.0, 15);
            var puls = compute.Add(12.0, 15);
            Console.WriteLine(puls);

            compute.Minus(100, 34);
            var minus= compute.Minus(100, 34);
            Console.WriteLine(minus);

            compute.Mlutiply(11 , 6);
            var mlutiply= compute.Mlutiply(11,6);
            Console.WriteLine(mlutiply);

            compute.Divide(10 , 50);
            var divide= compute.Divide(10 ,50);
            Console.WriteLine(divide);

        }
    }
}
