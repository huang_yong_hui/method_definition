﻿using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            TextOne textOne = new TextOne();

            var add=textOne.Add(2.5,2.6);

            var div=textOne.Div(5.5, 1.5);

            var mul=textOne.Mul(5, 2);

            var rem=textOne.Rem(6, 4);

            var sub=textOne.Sub(6, 5);

            Console.WriteLine("加法为：{0}减法为：{1}乘法为：{2}除法为：{3}取余为：{4}",add,sub,mul,div,rem);
        }
    }
}
