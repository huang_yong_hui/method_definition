﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    class TextOne
    {
        //加法
        public double Add(double x,double y)
        {
            return x + y;
        }


        //减法
        public int Sub(int x,int y)
        {
            return x - y;
        }

        //乘法
        public int Mul(int x,int y)
        {
            return x * y;
        }

        //除法
        public double Div(double x,double y)
        {
            return x / y;
        }

        //取余
        public int Rem(int x,int y)
        {
            return x % y;
        }

    }
}
