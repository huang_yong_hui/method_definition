﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 方法定义
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("输入第一个数字:");
            string a = Console.ReadLine();
            char c = char.Parse(a);
            Console.Write("输入第二个数字:");
            string b = Console.ReadLine();
            char d = char.Parse(b);
            Console.Write(Environment.NewLine);
            int addNum = c + d;
            int subNum = c - d;
            int purNum = c * d;
            int divNum = c / d;
            Console.WriteLine("两大数相加结果为：{0}", addNum);
            Console.WriteLine("两大数相减结果为：{0}", subNum);
            Console.WriteLine("两大数相乘结果为：{0}", purNum);
            Console.WriteLine("两大数相除结果为：{0}", divNum);
            Console.ReadLine();
        }
    }
}
