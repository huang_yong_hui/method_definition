﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
   public class Compute
    {
        /*加法*/
        public double Add(double number1, double number2)
        {
            return number1 + number2;
        }
        /*减法*/
        public double Minus(double number3, double number4)
        {
            return number3 - number4;
        }

        

        /*乘法*/
        public double Multiply(double number5, double number6)
        {
            return number5*number6;
        }
        /*除法*/
        public double Divide(double number7, double number8)
        {
            return number7 / number8;
        }
    }
}
