﻿using System;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Compute compute = new Compute();
            var res1 = compute.Add(34.2, 56);
            Console.WriteLine(res1);
            var res2 = compute.Minus(45, 34);
            Console.WriteLine(res2);
            var res3 = compute.Multiply(25,24);
            Console.WriteLine(res3.ToString());
            var res4 = compute.Divide(89.3,10);
            Console.WriteLine(res4.ToString());

        }
    }
}
