﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Compute compute = new Compute();

            var add = compute.Add(3.3 , 5);

            Console.WriteLine(add);

            var minus = compute.Minus(5, 3);

            Console.WriteLine(minus);

            var multipy = compute.Multiply(3, 9);

            Console.WriteLine(multipy);

            var divide = compute.Divide(9, 3);

            Console.WriteLine(divide);
        }
    }
}
