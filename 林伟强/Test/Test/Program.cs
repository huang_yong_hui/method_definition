﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Compute compute = new Compute();
            double num1;
            double num2;
            num1 = 20.0;
            num2 = 10.08;

            //加法
            var add = compute.Add(3.0, 6);
            Console.WriteLine("{0}+{1}={2}",3.0,6,add);
            //减法
            var min = compute.Min(5.4 , 2.8);
            Console.WriteLine("{0} + {1} = {2}",5.4,2.8,min);
            //乘法
            var mul = compute.Multiply(num1, num2);
            Console.WriteLine("{0} + {1} = {2}",num1,num2,mul);
            //除法
            var div = compute.Divide(num1, num2);
            Console.WriteLine("{0} + {1} = {2}",num1, num2,Math.Round(div,2));


        }
    }
}
